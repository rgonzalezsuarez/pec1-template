# PEC1 - Duelo de insultos
## ¡Duelo de insultos!

En "Duelo de insultos" encarnamos a un pirata que quiere llegar a lo mas alto, pero en nuestro camino se ha interpuesto otro aspirante, un infame llamado "Guybrush Threepwood", ¡acaba con él para ser un SwordMaster!

## Manual de uso
En el juego, cada pirata debe insultar al otro, y éste último debe contestarle de manera apropiada, solo hay una respuesta acertada a cada insulto. Si un pirata contesta correctamente, pasa a ganar un punto y a insultar él, si falla el pirata que insulta gana el punto y sigue insultando. Quien obtiene 3 puntos gana el combate.
En el juego el primer pirata en insultar es escogido de manera aleatoria:
##### Como insultar o contestar
Puedes elegir un insulto del listado y ver la contestación (en violeta) del enemigo, y verás tu insulto en verde. De igual manera si el enemigo te insulta (en violeta) podrás elegir una respuesta. 
En amarillo se muestran los mensajes de sistema de quien toca el insulto.

##### Dificultad
Existe un selector de dificultad, donde a mayor dificultad (Facil - Dificil), tu enemigo pasará a tener mas probabilidades a elegir una respuesta correcta. Si juegas en modo Pesadilla, tu oponente __SIEMPRE__ contestará correctamente, modo sólo para los mas expertos.


## Explicación de la estructura

El juego se compone de 3 escenas, donde la mas importante es el __Partida Nueva__ donde explicaremos brevemente como está implementado:

### componentes
 - DropDown: selector de dificultad del juego, integrado dentro de __GameplayManager__
 - Sección historia: contiene los gráficos para mostrar el estado del juego
 - puntuación Jugador y puntuación IA: textos que muestran los puntos de cada pirata en todo momento
 - Sección respuesta: parte para mostrar los botones de insultos/respuestas
 - Pause Screen: ventana de pausa con botones de reinicio/salir a menu
 - GamePlayeManager: objeto que contiene el script con la lógica de todo el juego
 - PauseScreenManager: lógica de pausa
 - fondo, player y back-main: gráficos del juego
 
#### GamePlayManager
 El script de lógica se compone de una clase StoryNode (que contiene la información del nodo actual de juego) un enumerado de Dificultad y las funciones de juego.
 
 La lógica de juego en el script es la siguiente:
 
 - __start__: funcion inicial que inicializa estados del nodo actual (por __FillStory__) e invoca a __FillUI__
 - __FillUI__: inicia los textos según el nodo actual, coloca los botones (de insultos o respuestas) y coloca los triggers en dichos botones a la funcion __AnswerSelected__
 - __AnswerSelected__:  Lógica de cuando el jugador selecciona un botón, si el jugador insulta, se invoca a __IAResponse__ , si la IA insulta se verifica si el jugador contestó correctamente. Independientemente de quien contestara, se verifica si alguien ha ganado para pasar a la escena final o seguir jugando, generando un nuevo nodo con __CreateNode__
 - __IAresponse__: Función que calcula cual es la respuesta correcta y prepara el sistema para elegir. Para separar la lógica de decisión, se llama a __selectAnswer__
 - __selectAnswer__: Lógica de decisión, según la dificultad elegida, calcula por probabilidad la respuesta correcta.

### Agradecimientos

#### Fuente
https://www.1001fonts.com/8-bit-limit-font.html
Made by Ænigma Fonts
Free for commercial use

#### Música Juego
https://incompetech.com/wordpress/2016/04/thief-in-the-night/
"Thief in the Night" de https://incompetech.com/music/
Thief in the Night Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

#### Música Menu Inicio
March of the Spoons de https://incompetech.com/music/
March of the Spoons Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

#### Música Menu Final
Beach Party de https://incompetech.com/music/
Beach Party Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

#### Gráficos
Gráficos del juego original editados mi.

# Bugs conocidos:
- El selector de dificultad se inicia en negro a pesar de tener una dificultad Fácil seleccionada (solo ocurre en la Build, imposible de debugear en el editor)