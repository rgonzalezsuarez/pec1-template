﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour {

    /// <summary>
    /// enumerado que indica la seleccion de dificultad del jugador, cada dificultad es una probabilidad de
    /// elegir la respuesta correcta (probabilidad sobre 100). Esta probabilidad se calcula como un peso a la hora de elegir
    /// respuesta, es decir se puede cambiar de dificultad en cualquier momento
    /// </summary>
    public enum Difficult {
        Easy = 1,
        Medium = 50,
        Hard = 90,
        Nightmare = 100
    };

    public class StoryNode {
      
        /// <summary>
        /// enumerado que indica el jugador que está insultando en el nodo
        /// </summary>
        public enum PlayerType { Player, IA };


        /// <summary>
        /// El texto que describe la localización.
        /// </summary>
        public string history;
        /// <summary>
        /// La lista de textos con las respuestas posibles.
        /// </summary>
        public string[] answers;
        /// <summary>
        /// Desde que las respuesta no son nodos a los que ir, sino solo hay
        /// una respuesta correcta, nextNode se pasa a correctAnswer,
        /// un array de booleanos que indicara cual es la respuesta correcta, en caso
        /// de ser un insulto al jugador 
        /// </summary>
        public bool[] correctAnswer;

        /// <summary>
        /// Indica quien es el que lleva el insulto en el nodo actual
        /// </summary>
        public PlayerType actualPlayer;



    }

    /// <summary>
    ///  indicará en todo momento en qué nodo se encuentra actualmente el jugador.
    /// </summary>
    private StoryNode current;

    public Text historyText;
    public Transform answersParent;
    public GameObject buttonAnswerPrefab;
    //puntaciones por jugador
    private int playerScore = 0;
    public Text playerScoreText;
    private int computerScore = 0;
    public Text computerScoreText;
    //configuracion de puntuación máxima, para una posible futura edición de puntos
    public int maxScore = 3;
    // dificultad actual seleccionada
    public Difficult difficult = Difficult.Easy;
    //selector de dificultad
    public Dropdown difficultySelector;

    //entidad con sprite y animaciones del combate y su componente animador
    public GameObject playerSprite;
    private Animator figthAnimator;
       
    //ventana de fin de juego
    public GameObject winScreen;
    //ganador del juego, guardado para pasar entre escenas esta información
    private StoryNode.PlayerType winner;

    private void Start()
    {
        StoryFiller storyF = this.GetComponent<StoryFiller>();
        current = storyF.FillStory();
        historyText.text = "";
        figthAnimator = playerSprite.GetComponent<Animator>();
        figthAnimator.SetBool("fighting", false);
        FillUI();
        //se asocia al selector de dificultad una función que cambia el comportamiento de la IA
        difficultySelector.onValueChanged.AddListener(delegate
        {
            DifficultyChange(difficultySelector);
        });
    }

     /// <summary>
     /// Selector de dificultad, que setea la dificultad elegida a una variable, para que la IA la consulte
     /// a cada contestación que da 
     /// </summary>
     /// <param name="difficultySelector">Dificultad seleccionada en el dropdown</param>
    private void DifficultyChange(Dropdown difficultySelector)
    {
        switch (difficultySelector.value)
        {
            case 0:
                this.difficult = Difficult.Easy;
                break;
            case 1:
                this.difficult = Difficult.Medium;
                break;
            case 2:
                this.difficult = Difficult.Hard;
                break;
            case 3:
                this.difficult = Difficult.Nightmare;
                break;
        }
    }

    void FillUI()
    {
        // Añadimos un par de lineas en blanco y el texto corresponiendte al nodo actual
        if (current.actualPlayer == StoryNode.PlayerType.IA)
        {
            //aqui solo indicara si la IA inicia un insulto
            historyText.text += "\n\n <color=yellow>el contrincante dice</color> \"<color=purple>" + current.history + "</color>\"";
        } else
        {
            historyText.text += current.history;
        }
        // Borramos los anteriores botones
        foreach (Transform child in answersParent.transform)
        {
            Destroy(child.gameObject);
        }
                
        bool isLeft = true;
        float height = 430;
        int index = 0;
        // Colocamos los nuevos botones
        // Por cada respuesta crearemos un botón
        foreach (string answer in current.answers)
        {
            // Creamos el botón y lo asociamos al enemento de la UI correspondiente.
            GameObject buttonAnswerCopy = Instantiate(buttonAnswerPrefab);
            buttonAnswerCopy.transform.parent = answersParent;

            // Le damos la posición adecuada según pertoque
            float x = buttonAnswerCopy.GetComponent<RectTransform>().rect.x * 1.3f;
            buttonAnswerCopy.GetComponent<RectTransform>().localPosition = new Vector3(isLeft ? x : -x, height, 0);

            // Si es el de la derecha, sumamos distancia para colocarlos en la siguiente línea
            if (!isLeft)
                height += buttonAnswerCopy.GetComponent<RectTransform>().rect.y * 3.0f;
            isLeft = !isLeft;

            // Asociamos
            FillListener(buttonAnswerCopy.GetComponent<Button>(), index);

            // Rellenamos el texto del botón
            buttonAnswerCopy.GetComponentInChildren<Text>().text = answer;
 
            index++;
        }
    }



    /// <summary>
    /// Esta función asocia el hacer click en el botón, con la función 'AnswerSelected'.
    /// </summary>
    /// <param name="button">Botón sobre el que se añadirá la acción.</param>
    /// <param name="index">posición del botón dentro de la lista de respuestas.</param>
    void FillListener(Button button, int index)
    {
        button.onClick.AddListener(
            () => {
                AnswerSelected(index);
            }
            );
    }


    /// <summary>
    ///la IA esta eligiendo un item por probabilidad, esta funcion calcula por peso (segun la dificultad)
    ///la respuesta correcta (pasada por parametro)
    /// </summary>
    /// <param name="correctAnswer">indica cual es la respuesta correcta al insulto</param>
    /// <returns></returns>
    private int selectAnswer(int correctAnswer)
    {
        //se elige las respuestas a insultos, se crea una lista por probabilidades
        int numAnswers =  this.GetComponent<StoryFiller>().baseNode.answers.Length;
        int selectedElement = 0;
        List<KeyValuePair<int, double>> elements = new List<KeyValuePair<int, double>>();
        //rellenamos la lista de probabilidades
        for (int i = 0; i < current.answers.Length; i++)
        {
            if (i == correctAnswer)
            {
                //la probabilidad especificada en el Enum entre 100
                elements.Add(new KeyValuePair<int, double>(i, (double)(int) difficult / (double)100));
            } else
            {
                //probabilidad de 1 entre todas las respuestas
                elements.Add(new KeyValuePair<int, double>(i, (1.0 -((double)(int)difficult / (double)100)) / (double) numAnswers));
            }
        }
        // el calculo es una probabilidad acumulativa
        System.Random r = new System.Random();
        double diceRoll = r.NextDouble();

        double cumulative = 0.0;
        for (int i = 0; i < elements.Count; i++)
        {
            cumulative += elements[i].Value;
            if (diceRoll < cumulative)
            {
                selectedElement = elements[i].Key;
                break;
            }
        }
        return selectedElement;
    }

    /// <summary>
    /// logica de la IA, modular para poder modificar y agregar futura IA de verdad 
    /// llama al metodo SelectAnswer que tiene la lógica de selección de la respuesta, el resto
    /// del código es la lógica que prepara el entorno para elegir respuesta
    /// </summary>
    /// <param name="index">es el indicado del insulto elegido por el jugador</param>
    /// <returns>indica si la IA acertó la respuesta</returns>
    bool IAresponse(int index) {
        //respuesta correcta al insulto
        int correctAnswer = 0;
        //respuesta que elige la IA
        int selectedAnswer = 0;

        bool isCorrect = false;
        //precalculamos la respuesta correcta
        //ahora buscamos por el texto el insulto general en el objecto de configuracion para obtener cual es la respuesta correcta
        StoryFiller.NodeConf conf = this.GetComponent<StoryFiller>().baseNode;
        for (int i = 0; i < conf.insults.Length; i++)
        {
            if (conf.insults[i].insult == current.answers[index])
            {
                //insulto del jugador, extraer respuesta correcta
                correctAnswer = conf.insults[i].answer;
                break;
            }
        }

        if (difficult == Difficult.Easy || difficult == Difficult.Medium || difficult == Difficult.Hard)
        {
            //facil, medio y dificil son probabilidades en un modo aleatorio, se genera un sistema de probabilidades acumulativas
            selectedAnswer = selectAnswer(correctAnswer);
            if (selectedAnswer == correctAnswer)
            {
                isCorrect = true;
            }
        } else
        {
            //Nightmare - se elige siempre la respuesta correcta sin calculo, para evitar 
            //calcular probabilidades cuando se va a tener un 100% de acierto
            selectedAnswer = correctAnswer;
            isCorrect = true;
        }
        //una vez elegida la respuesta, rellenar y generar nuevo Nodo
        historyText.text += "\n <color=yellow>el contrincante contesta</color> \"<color=purple>" + conf.answers[selectedAnswer]+ "</color>\"";
        return isCorrect;
    }

    /// <summary>
    /// Esta es la función a la que se llamará cuando se toque el botón.
    /// Si se pulsa un boton y el jugador lleva el insulto, se pasa a la respuesta de IA al insulto
    /// Si se pulsa un boton y la IA lleva el insulto, se pasa a ver si la respuesta es correcta
    /// </summary>
    /// <param name="index">posición del botón dentro de la lista de respuestas.</param>
    void AnswerSelected(int index)
    {
        // Añadimos la respuesta seleccionada a la historia
        historyText.text += "\n <color=yellow>el jugador dice</color> \"<color=green>" + current.answers[index]+ "</color>\"";
        StoryNode.PlayerType nextPlayer = StoryNode.PlayerType.IA; //el proximo jugador que va a insultar
        //verificando quien lleva el insulto para decidir la lógica
        switch (current.actualPlayer) {
            case StoryNode.PlayerType.Player:
                //el jugador esta eligiendo un insulto
                if (IAresponse(index))
                {
                    this.computerScore++;
                    this.computerScoreText.text = this.computerScore.ToString();
                    nextPlayer = StoryNode.PlayerType.IA;               
                   
                } else
                {
                    //IA falla, siguiente insulto para Jugador humano
                    this.playerScore++;
                    this.playerScoreText.text = this.playerScore.ToString();
                    nextPlayer = StoryNode.PlayerType.Player;
                }
                 
                break;
            case StoryNode.PlayerType.IA:
                //el jugador responde a un insulto
                if (current.correctAnswer[index]) {

                    this.playerScore++;
                    this.playerScoreText.text = this.playerScore.ToString();
                    nextPlayer = StoryNode.PlayerType.Player; //jugador contesta correctamente, pasa a insultar
                   
                } else
                {
                    //jugador falla, sigue insultando IA
                    this.computerScore++;
                    this.computerScoreText.text = this.computerScore.ToString();
                    nextPlayer = StoryNode.PlayerType.IA;
                }

                break;
        }
        //se anima un combate

        figthAnimator.SetBool("fighting", true);
        //se verifica si ha ganado alguien, para pasar a cierre de juego o seguir jugando 
        if (this.computerScore == this.maxScore)
        {
            //IA gana
            historyText.text += "\n <color=yellow>El contrincante gana!</color>";
            this.winner = current.actualPlayer;
            PlayerPrefs.SetString("Winner", "IA");
            SceneManager.LoadScene("Fin Juego");
        }
        else if (this.playerScore == this.maxScore)
        {
            //jugador gana
            historyText.text += "\n <color=yellow>el Jugador gana!</color>";
            this.winner = current.actualPlayer;
            PlayerPrefs.SetString("Winner", "Player");
            SceneManager.LoadScene("Fin Juego");
        }
        else
        {
            //se sigue el juego
            current = this.GetComponent<StoryFiller>().CreateNode(nextPlayer);
            FillUI();
        }
    }

    /// <summary>
    /// Calculo de animación
    /// </summary>
    private void Update()
    {
        if (figthAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.name == "figth")
        {
            if (!figthAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.isLooping)
            {
                figthAnimator.SetBool("fighting", false);
            }
        }
    }

}
