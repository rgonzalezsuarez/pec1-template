﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // <-- Esta librería es necesaria para poder usar directamente SceneManager

public class PauseScreenManager : MonoBehaviour {

    public GameObject pauseScreen;

    public void LoadStartScene()
    {
        SceneManager.LoadScene("Pantalla Inicial");
    }

    public void RestartScene()
    {
        SceneManager.LoadScene("Partida Nueva");
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
            pauseScreen.SetActive(!pauseScreen.activeSelf);
    }

}
