﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// La clase de StoryFiller se ha cambiado a MonoBehaviour para poder mantener en memoria
/// el objeto de configuración baseNode (y así no estar cargando el JSON a cada creación),
/// ha sido cuestión de elegir si cargar JSON repetidas veces o mantenerlo en memoria simplemente.
/// </summary>
public class StoryFiller : MonoBehaviour {

    
    /**
     * Clase para deserializar la carga de los textos del JSON
     * la clase NodeConf, sirve para carga la información de preguntas con sus respuestas:
     * NOTA: se ha dejado (aunque se deberia limpiar) a modo demostrativo el ToString usado para depurar la clase
     */
    [System.Serializable]
    public class NodeConf {
        public Insult[] insults; //array de insultos con su respuesta correcta
        public string[] answers; //array de respuestas disponibles
    }
    [System.Serializable]
    public class Insult {
        public string insult;
        public int answer;
    }

    //objeto de configuracion del juego
    public NodeConf baseNode;



    /// <summary>
    ///  Crea un nodo con toda la informacion del estado actual de la partida, se genera
    ///  segun el jugador o la IA lleva el insulto en este turno
    /// </summary>
    /// <param name="playerType"> enumerado que indica quien está insultando actualmente, para generar el nodo, si la IA o el jugador</param>
    /// <returns></returns>
    public GameplayManager.StoryNode CreateNode(GameplayManager.StoryNode.PlayerType playerType)
    {
        GameplayManager.StoryNode node = new GameplayManager.StoryNode();
        node.actualPlayer = playerType;
        switch (playerType) {
            case GameplayManager.StoryNode.PlayerType.Player:
                //el jugador insulta, debe elegir insultos
                //la IA debera elegir una respuesta, segun la opcion
                //del jugador, no tiene sentido un array de correctAnswers ahora
                node.history = "\n\n <color=yellow>¡te toca insultar!</color>";
                node.answers = new string[this.baseNode.insults.Length];
                for (int i = 0; i < this.baseNode.insults.Length; i++) {
                    node.answers[i] = this.baseNode.insults[i].insult;
                }
                
                break;
            case GameplayManager.StoryNode.PlayerType.IA:
                //la IA insulta, hay que generar el insulto
                //el jugador elige respuestas que se mantienen en correctAnswer

                Insult insult = this.baseNode.insults[Random.Range(0, this.baseNode.insults.Length - 1)];
                node.history = insult.insult;
                //generar un array de respuestas, y en el mismo orden un array que indica si es verdadera o falso
                node.answers = new string[this.baseNode.answers.Length];
                //inicializar un array lo declara entero a false
                node.correctAnswer = new bool[this.baseNode.answers.Length];
                node.answers = this.baseNode.answers;
                //en insult.answer se tiene el index de la respuesta correcta, lo inicializamos en el array
                node.correctAnswer[insult.answer] = true;
                break;

        }
        return node;
    }

    /// <summary>
    /// StoryFiller inicializa el objeto de configuración
    /// </summary>
    public GameplayManager.StoryNode FillStory()
    {
        string texto2 = Resources.Load<TextAsset>("test").text;
        this.baseNode = JsonUtility.FromJson<NodeConf>(texto2);
        //empezamos con un jugador o IA aleatoriamente
        System.Array players = System.Enum.GetValues(typeof(GameplayManager.StoryNode.PlayerType));
        return this.CreateNode((GameplayManager.StoryNode.PlayerType)players.GetValue(Random.Range(0,players.Length)));
    }
}
