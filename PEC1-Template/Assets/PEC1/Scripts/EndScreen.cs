﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    public Text endText;
	// Use this for initialization
	void Start () {
        Debug.Log("ganador:");
        String winner = PlayerPrefs.GetString("Winner");
        Debug.Log(winner);
        if (winner == "Player")
        {
            endText.text = "¡Enhorabuena! ¡Has ganado el duelo!";
        } else
        {
            endText.text = "¡Has perdido el duelo!";
        }
        PlayerPrefs.DeleteAll();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
